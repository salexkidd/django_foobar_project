# 1st

```
$ export DJANGO_SETTINGS_MODULE="foobar.settings.development"
```

or

```
$ ./manage.py brahbrahbrah --settings=foobar.settings.development
```


# 2nd

```
$ cd ~/
$ git clone git@bitbucket.org:salexkidd/django_foobar_project.git
$ cd ~/django_foobar_project/foobar
$ pip install -r ./requirements.txt
$ ./manage.py migrate
$ ./manage.py runserver 0.0.0.0:8000
```

please open http://127.0.0.1:8000 with GoogleChrome or Safari. IE or Edge? I don't know :P
