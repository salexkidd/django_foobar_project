from django.db import models as dj_models


class CompanyOnlyDemo(dj_models.Model):

    name = dj_models.CharField(
        max_length=200,
        null=False,
        blank=False,
    )
