from django.contrib import admin

from . import models as company_only_apps_models


admin.site.register(company_only_apps_models.CompanyOnlyDemo)
