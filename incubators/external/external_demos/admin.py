from django.contrib import admin

from . import models as external_demo_models


admin.site.register(external_demo_models.ExternalDemo)
