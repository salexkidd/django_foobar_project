from django.db import models as dj_models


class ExternalDemo(dj_models.Model):

    name = dj_models.CharField(
        max_length=200,
        null=False,
        blank=False,
    )
