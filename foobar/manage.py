#!/usr/bin/env python
import os
import sys


try:
    BASE_DIR = os.path.dirname(os.path.dirname(__file__))
    INCUBATORS_DIR = os.path.join(BASE_DIR, "../incubators/")

    for p in ["internal", "external",]:
        sys.path.append(os.path.join(INCUBATORS_DIR, p))


except Exception as e:
    raise AssertionError("Can't find incubators")

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "foobar.settings")

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
