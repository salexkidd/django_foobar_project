from django.db import models as django_models
from django.utils.translation import ugettext as _


class AbstractModel(django_models.Model):

    create_datetime = django_models.DateTimeField(
        null=False,
        blank=False,
        auto_now_add=True,
        auto_now=False,
        verbose_name=_("create_datetime"),
        help_text=_("作成日"),
    )

    update_datetime = django_models.DateTimeField(
        null=False,
        blank=False,
        auto_now=True,
        verbose_name=_("更新日"),
        help_text=_("更新日"),
    )

    class Meta:
        abstract = True
