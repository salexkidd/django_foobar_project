from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext as _


class CustomUserAdmin(UserAdmin):
    fieldsets = UserAdmin.fieldsets + (
        (
            _("Description"), {
                'fields': ('additinal_description',)}
        ),
    )

admin.site.register(get_user_model(), CustomUserAdmin)
