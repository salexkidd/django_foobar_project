from django.db import models
from django.contrib.auth import models as auth_models
from django.utils.translation import ugettext as _


class User(auth_models.AbstractUser):

    additinal_description = models.TextField(
        null=True,
        blank=True,
        help_text=_("Hola!! This is Additonal field!Please input description"),
        verbose_name=_("description")
    )
