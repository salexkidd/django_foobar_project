from django.contrib import admin

from . import models as polls_models


class ChoiceInline(admin.StackedInline):
    model = polls_models.Choice
    extra = 3


class PollAdmin(admin.ModelAdmin):

    list_display = [
        'question',
        'pub_date',
        'create_datetime',
        'update_datetime',
    ]

    fieldsets = [
        (
            None,
            {
                'fields': ['question']
            }
        ),

        (
            'Date information',
            {
                'fields': ['pub_date'],
                'classes': ['collapse']
            }
        ),
    ]
    inlines = [ChoiceInline]


admin.site.register(polls_models.Poll, PollAdmin)
