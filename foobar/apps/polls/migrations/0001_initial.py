# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Choice',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('create_datetime', models.DateTimeField(auto_now_add=True, verbose_name='create_datetime', help_text='作成日')),
                ('update_datetime', models.DateTimeField(verbose_name='更新日', auto_now=True, help_text='更新日')),
                ('choice', models.CharField(max_length=200)),
                ('votes', models.IntegerField(default=0)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Poll',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('create_datetime', models.DateTimeField(auto_now_add=True, verbose_name='create_datetime', help_text='作成日')),
                ('update_datetime', models.DateTimeField(verbose_name='更新日', auto_now=True, help_text='更新日')),
                ('question', models.CharField(max_length=200)),
                ('pub_date', models.DateTimeField(verbose_name='date published')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='choice',
            name='poll',
            field=models.ForeignKey(to='polls.Poll'),
        ),
    ]
