from django.db import models as django_models
from libs.abstract_model.models import AbstractModel
from django.utils.translation import ugettext as _


class Poll(AbstractModel):
    question = django_models.CharField(
        max_length=200,
        null=False,
        blank=False,
    )
    pub_date = django_models.DateTimeField(
        'date published'
    )


class Choice(AbstractModel):
    poll = django_models.ForeignKey(
        "Poll",
        null=False,
        blank=False
    )
    choice = django_models.CharField(
        max_length=200,
        null=False,
        blank=False,

    )
    votes = django_models.IntegerField(
        null=False,
        blank=False,
        default=0,
    )
